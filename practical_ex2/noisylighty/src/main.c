/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
/* FIXME: We should use angle brackets here because string.h is from the clib
 *  i.e., not part of this project but some dependency */
#include <string.h> // OK :)


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
// for the USART Output
char send[50] = {};
//------Buzzer and Photores----------------------

// variable for storing the incomig value
uint32_t inputValue = 0;

// variable for level of noise 
uint16_t noiseLvl = 0;

//------LEDs----------------------------------

// const array of uint16_t for scanner
uint16_t const led[] = { LED1_Pin, LED2_Pin, LED3_Pin, LED4_Pin, LED5_Pin, LED6_Pin, LED7_Pin, LED8_Pin };

// const array of pinmodes
GPIO_TypeDef* const modeOfPins[] = { GPIOB, GPIOB, GPIOF, GPIOA, GPIOB, GPIOB, GPIOA, GPIOA };

//var for current led in the for loop
int currentLed=0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

// The clean new function
void calcNoiseLevel(uint32_t input)
{
 noiseLvl = 20 - 4*((inputValue/1000)) ; // our max value 20 minus 4 bits * output divided by 1000 
}

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_ADC_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */

  // Setup all Moduls
  HAL_GPIO_TogglePin(GPIOA,ADC_INPUT_Pin); 
  HAL_ADCEx_Calibration_Start(&hadc);
  //HAL_UART_Transmit(&huart2,startMessage,strlen(startMessage),1000); 

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    //ADC
    HAL_ADC_Start(&hadc);
    HAL_ADC_PollForConversion(&hadc,1000);
    inputValue = HAL_ADC_GetValue(&hadc); // get the value of photores
    HAL_ADC_Stop(&hadc);
   // ADC END

    
    /* FIXME: I don't think that we get numbers like 19 with the following formula
     *  as the expression (outputValue/1000) will always be an integer (and it
     *  should be an integer not a float or double). --> OK  i got it!
     *
     *  You will loose quite a bit of resolution here. To be honest, I don't know
     *  if this matters for the noise level. Suppose however that it matters,
     *  how would the formula look like? Tip: Extract this into an own function
     *  and test it on the development computer directly. Or use  a
     *  online compiler for crafting the function https://www.onlinegdb.com/online_c_compiler.
     *
     * When done, you could also output the value on the serial wire. <--- ?
     * 
     * But in the practical-ex2 PDF 3. g) you wrote:
     * "...Die Ausgabe auf der seriellen Schnittstelle kann dann entfernt werden."
     * But added it!
     * 
    */
    // Our new function to calculate the noise level
    calcNoiseLevel(inputValue);

    // USART Output
     HAL_UART_Transmit(&huart2, send,snprintf(&send,sizeof(send), " Noise value [ %d ] \r\n",(noiseLvl)),1000); 

    //MAKRO to send the sound
    __HAL_TIM_SET_COMPARE(&htim14,TIM_CHANNEL_1,noiseLvl); 
    HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
    HAL_Delay(50); 
    
    
    for(currentLed = 7; currentLed >= 0; currentLed-- )
    {
        if(currentLed <= (noiseLvl)/2) 
        { 
          //we use HAL_GPIO_Togglepin 
          HAL_GPIO_WritePin(modeOfPins[currentLed],led[currentLed],GPIO_PIN_SET); // enable current pin when if statement true
        }
        else
        {
          HAL_GPIO_WritePin(modeOfPins[currentLed],led[currentLed],GPIO_PIN_RESET); // disable current pin when else
        }
    }
    /* USER CODE END WHILE */


    /* USER CODE BEGIN 3 */
  
 } /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
