#include "led.h"

void led_toggle(void)
{
    static volatile uint32_t* const GPIOB_ODR = (uint32_t*)(GPIOB_BASE + 0x14);
    *GPIOB_ODR ^= 0x00000008;
}

void led_set(uint8_t value)
{
    static volatile uint32_t* const GPIOB_ODR = (uint32_t*)(GPIOB_BASE + 0x14);
	switch (value)
	{
		case 1 : 
			*GPIOB_ODR |= 0x00000008;
			break;

		default:
			*GPIOB_ODR &= ~(0x00000008);
			break;
	}
}

void led_init()
{
	/* activate GPIOB */
    static volatile uint32_t* const RCC_AHBENR = (uint32_t*)(RCC_BASE + 0x14);
    *RCC_AHBENR |= 0x00040014;

    /* activate PIN 3 */
    static volatile uint32_t* const GPIOB_MODER = (uint32_t*)(GPIOB_BASE + 0x00);
    *GPIOB_MODER |= 0x00000040;
}