#include "led.h"

/**
 * \brief this function is for the interrupt
 */
void systick(void);

/**
 * \brief this function sets all systick interrupt registers
 */
void systick_init();

/**
 * \brief this function tells C to wait for interrupts
 */
void wfi();

void _start()
{	
	led_init(); /* init. the led with all registers*/
	systick_init(); /* init the systick*/

	/* while loop to make it blink */
	 while(1)
	{
        wfi();
    }
}

void systick_init()
{
	static volatile uint32_t* const STK_CSR = (uint32_t*)(0xE000E010);
    static volatile uint32_t* const STK_RVR = (uint32_t*)(0xE000E014);
    static volatile uint32_t* const STK_CVR = (uint32_t*)(0xE000E018);
    static volatile uint32_t* const STK_CALIB = (uint32_t*)(0xE000E01C);
	*STK_RVR = 0x3D0900; /*rate 0.5 seconds */
    *STK_CVR |= 0x10; /* clear register */           
    *STK_CSR |= 0x7;  /*enable bit 1 & 2*/
};

void systick(void)
{
	led_toggle();
}

void wfi(){
    __asm ("    WFI\n");
    return;
}
