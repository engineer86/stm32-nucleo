#ifndef BLINKYPRIME_LED_H
#define BLINKYPRIME_LED_H

#include <stdint.h>

#define RCC_BASE 0x40021000
#define GPIOB_BASE 0x48000400
#define GPIOA_BASE 0x48000000

/**
 * \brief this function is setting the LED GPIOB for the LED
 * \param value is for ON (1) or OFF (0)
 */ 
void led_set(uint8_t value);

/**
 * \brief this function is toggle the LED GPIOB for the LED
 */ 
void led_toggle(void);

/**
 * \brief this function is activating the GPIOB @ PIN3
 * */
void led_init();

#endif