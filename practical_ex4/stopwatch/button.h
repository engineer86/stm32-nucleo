#ifndef STOPWATCH_BUTTON_H
#define STOPWATCH_BUTTON_H

#include <stdint.h>
#include "stdbool.h"
#define GPIOA_BASE 0x48000000
#define RCC_BASE 0x40021000

/**
 * \brief this function is reading the value of the PIN
 *  which is connected to the button, it returns TRUE for a HIGH
 *  and FALSE for a LOW signal
 */
bool button_read_value();

/**
 * \brief this function init. registers for the button
 * */
void button_init();

#endif