.global _start
.global systick

.syntax unified

Vectors:
    .word 0x20001800
    .word _start + 1
    .org 0x3C
    .word systick + 1
