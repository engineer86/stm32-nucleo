#ifndef STOPWATCH_USART_H
#define STOPWATCH_USART_H

/*DEFINES*/
#define ISR_TXE (1<<7) /* Transmit data register empty */
#define ISR_TC (1<<6) /* Transmission complete */
#define RCC_BASE 0x40021000
#define USART2_BASE 0x40004400
#define GPIOA_BASE 0x48000000

#include <stdint.h>

/**
 * \brief the function is sending one char
 * \param input parameter c represents the char
*/
void usart_putc(uint8_t c);

/**
 * \brief the function is sending one string pointer
 * \param input parameter str represents the string  
*/
void usart_puts(const char *str);

/**
 * \brief the function is sending ASCII - string
 * \param input parameter val represents the integer 
 */
void usart_putx(uint32_t val);

/**
 * \brief this function is setting up the whole USART for use
*/
void usart_init();

/**
 * \brief function to convert 
 * */
uint32_t convert(uint32_t value, char* buffer_ptr);

#endif