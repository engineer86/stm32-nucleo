#include "button.h"

bool button_read_value()
{	
	/*enable button  RM 8.4.5 INPUT DATA REGISTER */
    static volatile uint32_t* const GPIOA_IDR = (uint32_t*)(GPIOA_BASE + 0x10);
    if(*GPIOA_IDR & (1<<7))
	{
        return true;
    }
    else
	{
        return false;
    }
}

void button_init()
{   
	/* GPIOA enable RM 6.4.6 BIT 17 */
    static volatile uint32_t *const RCC_AHBENR = (uint32_t *)(RCC_BASE + 0x14);
    *RCC_AHBENR |= (1 << 17);
    
    /* change mode to input RM 8.4.1 */
    static volatile uint32_t *const GPIOA_MODER = (uint32_t *)(GPIOA_BASE + 0x00);
    *GPIOA_MODER &= ~(1<<14);

    /* set to pullup mode RM 8.4.4 */
    static volatile uint32_t* const GPIOA_PUPDR = (uint32_t*)(GPIOA_BASE + 0x0C);
    *GPIOA_PUPDR |= (1<<14);
}