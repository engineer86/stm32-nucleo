#ifndef STOPWATCH_H
#define STOPWATCH_H

#include "usart.h"
#include "led.h"
#include "button.h"
#include "utils.h"

 uint32_t seconds = 0;
 uint32_t milli_seconds = 0;
 uint8_t quarter_secs = 0;

 // bool flag which indicates if stopwatch started
static bool stopwatch_started = false;
 // bool flag which indicates if stopwatch has calculated the time
static bool stopwatch_calculated = false;

/**
 * \brief this function is for the interrupt
 */
void systick(void);

/**
 * \brief this function sets all systick interrupt registers
 */
void systick_init();

/**
 * \brief this function initializes all modules of the stopwatch
*/
void init_stopwatch_full();

/**
 * \brief this function is setting the systick state
 * \param current_state when true the timer starts, when false the timer stops
 */void systick_state(bool current_state);

/**
 * \brief this function tells C to wait for interrupts
 */
void wfi();

/**
 * \brief test function to send time via usart
 * */
void display_time();

/**
 * \brief reset the time vars of the stopwatch
 * */
 void reset_watch();

/**
 * \brief this function is starting the measurement of the stopwatch
 * in a endless loop (while(1))
 * */
void start_stopwatch();

#endif