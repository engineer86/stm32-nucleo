#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>

uint32_t milli_seconds;
uint32_t seconds;
uint32_t calc;

/**
 * \brief this function is calculating the halfseconds to milliseconds
 * \param cvr 
 * \param halfseconds
 * \return the function returns the value of milliseconds
 */
//uint32_t cvr2ms(const uint32_t cvr, uint32_t halfseconds);

uint32_t cvr2ms(uint32_t cvr, uint32_t halfseconds);

#endif