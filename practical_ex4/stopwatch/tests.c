#include <assert.h>
#include "utils.c" /* unit under test*/

int main()
{
	assert(cvr2ms(0,0) == 500);  
	assert(cvr2ms(0,1) == 1000); 
	assert(cvr2ms(0,2) == 1500); 
	assert(cvr2ms(2000000,0) == 250); 
	assert(cvr2ms(0,2000000) == 1000000500);
	assert(cvr2ms(500,1) == 999);
	assert(cvr2ms(1,-1) == -1);
	assert(cvr2ms(-1,1) == 1000);
	printf("Tests done. \r\n");
	
	return 0;
}