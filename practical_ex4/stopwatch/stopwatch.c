#include "stopwatch.h"

/* Strategy to measure the time :
The following registers are important for calculating the time: STK_RVR, STK_CVR and ISR.
The STK_RVR is set so that an interrupt is triggered every quarter of a second.
The ISR must keep the timer running, the number of these starts must be counted.
When the stopwatch is stopped we read the values ​​of the registers STK_RVR and STK_CVR the difference is the value of the systicks.
If you add the elapsed time to this, we get our timed time.
*/

/* main function */
void _start()
{	
	init_stopwatch_full();
	start_stopwatch();
}

/* functions */
void systick_init()
{
	//vars for registers
	static volatile uint32_t* const STK_CSR = (uint32_t*)(0xE000E010);
	static volatile uint32_t* const STK_RVR = (uint32_t*)(0xE000E014);
	static volatile uint32_t* const STK_CVR = (uint32_t*)(0xE000E018);

    *STK_RVR = 0x1E8480;   /*rate 0.5 seconds */    
    *STK_CVR = 0x10;       /* clear register */     
    *STK_CSR |= 0x6;      /*enable bit 1 & 2*/     	
}

void systick(void)
{	quarter_secs++;
    led_toggle();
}

void systick_state(bool current_state)
{
	static volatile uint32_t* const STK_CSR = (uint32_t*)(0xE000E010);
    static volatile uint32_t *const STK_CVR = (uint32_t *)(0xE000E018);

	if(current_state == true)
	{
		*STK_CVR = 0x10; 
        *STK_CSR |= 0x1; 
	}
	else
	{	
		static volatile uint32_t *const STK_CVR = (uint32_t *)(0xE000E018);
		seconds = quarter_secs * 0.25;
		quarter_secs = (quarter_secs % 4);
		uint32_t calc = (0xFFFFFFFF & *STK_CVR);            
    	calc = calc * 0.5;
		milli_seconds = ((quarter_secs * 250) + (1000 - calc));  
		*STK_CSR &= ~0x1;
    }
}

void wfi(){
    __asm ("    WFI\n");
    return;
}

void display_time()
{
    usart_puts("[stopped time: ");
    char buffer_sec[15];
    char* sec_ptr = &buffer_sec[0];
    uint8_t sec_count = convert(seconds, sec_ptr);
    usart_puts(sec_ptr);
    usart_putc(',');
    char buffer_ms[15];
    char* ms_ptr = &buffer_ms[0];
    uint8_t ms_count = convert(milli_seconds, ms_ptr);
    usart_puts(ms_ptr);
    usart_puts(" sec.]\r\n");
}

 void reset_watch()
 {
	 seconds = 0;
	 milli_seconds = 0;
	 quarter_secs = 0;
 }

 	
void init_stopwatch_full()
{
	usart_init(); // init usart from usart.h
	led_init(); // init led from led.h
	systick_init(); // intit systick from stopwatch.h
	button_init(); // init button from button.h
	led_set(1); // turn on led 
	usart_puts("All modules initialized !\r\n");// to test usart communication 
}

void start_stopwatch()
{
	while(1)
	{
		if(button_read_value() == false)
		{
			if(stopwatch_started == false)
			{
				stopwatch_started = true;
				stopwatch_calculated = false;
				led_set(0);
				systick_state(true);
				usart_puts("[Stopwatch start]\r\n");
			}
		}
		else
		{
			stopwatch_started = false;
			systick_state(false);
			led_set(1);

			if(stopwatch_calculated == false)
			{
				display_time(false);
				stopwatch_calculated = true;
				usart_puts("[Stopwatch stopped]\r\n\r\n"); 
				reset_watch(); 
			}
		}
	}
}