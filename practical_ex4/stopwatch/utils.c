#include "utils.h"


uint32_t cvr2ms(uint32_t cvr, uint32_t halfseconds)
{	
  uint32_t ticker = 4000000;
  uint32_t calc =  ticker - cvr;
  return (calc / 8000 + halfseconds * 500);
}