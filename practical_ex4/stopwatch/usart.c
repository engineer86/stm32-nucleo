#include "usart.h"

/* volatile: It tells the compiler that the value of the variable 
may change at any time--without 
any action being taken by the code the compiler finds nearby
*/

/* CR1 register USART2-Block */
static volatile uint32_t * const usart2_cr1 = (uint32_t*)(USART2_BASE + 0x00);

/* Interrupt and status register */
static volatile uint32_t *const usart2_isr = (uint32_t *)( USART2_BASE + 0x1c);

/* Transmit data register address */
static volatile uint32_t *const usart2_tdr = (uint32_t *)( USART2_BASE + 0x28);

void usart_putc(uint8_t c)
{
	while (!(*( usart2_isr) & ISR_TXE ));
	*usart2_tdr = c;
	while (!(*( usart2_isr) & ISR_TC ));
}

void usart_puts(const char* str)
{
	char c;
    while((c = *str++))
	{
        usart_putc(c);
    }
}

void usart_putx(uint32_t val)
{    
	uint32_t move = 0; /* var to move value*/
	
	/* right bitshift */
	for (int i = 7; i >= 0; i--) 
	{
		move = 0xF & (val >> (4 * (i)));

		/*if 65 = A >=10 */
		if((move+55)>=65)
		{
        	usart_putc(move + 55);
		}
		/* if copy == 0-9 -> numbers 48-57 */
		else 
		{
          	usart_putc(move + 48);
		}
	}
}

void usart_init()
{	/*activate the GPIOA clock*/
	static volatile uint32_t* const RCC_AHBENR = (uint32_t*)(RCC_BASE + 0x14);
    *RCC_AHBENR |= (1<<17);

    /*activate USART2 RCC block*/
    static volatile uint32_t* const RCC_APB1ENR = (uint32_t*)(RCC_BASE + 0x1C);
    *RCC_APB1ENR |= (1<<17);

	/* change mode AFR */
	static volatile uint32_t* const GPIOA_MODER = (uint32_t*)(GPIOA_BASE + 0x00);
        *GPIOA_MODER |= (1<<5);     /* PA2 = USART2_TX */
        *GPIOA_MODER |= (1<<31);    /* PA3 = USART2_RX */
	
 	/* enable AFRL PIN 0-7, AFRH PIN 8-15 */
        static volatile uint32_t* const GPIOA_AFRL = (uint32_t*)(GPIOA_BASE + 0x20);
        static volatile uint32_t* const GPIOA_AFRH = (uint32_t*)(GPIOA_BASE + 0x24);
        *GPIOA_AFRL |= 0x11111;        /* PA2 <= AF1 */
        *GPIOA_AFRH |= (1<<28);       /* PA15 <= AF1 */

    /* enable USART2_CR1 */
        static volatile uint32_t* const USART2_CR1 = (uint32_t*)(USART2_BASE + 0x00);
        *USART2_CR1 |= (1<<3);      /* TX ON */

    /* BAUDRATE */
    static volatile uint32_t* const USART2_BRR = (uint32_t*)(USART2_BASE + 0x0C);
    *USART2_BRR |= 0x0341; /*Example 1 from RM 27.5.4*/

	/*set to enable*/
    *USART2_CR1 |= (1<<0);  


}

uint32_t convert(uint32_t val, char* buffer_ptr)
{
    uint32_t size = 20;
    char buffer[size];
    uint32_t count = 0;

    if (val == 0)
    {
        *buffer_ptr = '0';
        *++buffer_ptr = '\0';
        buffer_ptr--;

        count = 2;
        return count;
    }
    else
    {
        uint32_t x = 0;

        do {
            x = val % 10;

            val -= x;
            val /= 10;

            *buffer_ptr++ = x + '0';

            count++;

        } while (val > 0);
        *buffer_ptr = '\0';
        count++;

        for(uint32_t i=0; i<count; i++){
            buffer[i] = *buffer_ptr--;
        }

        for(uint32_t i=0; i<count; i++){
            *buffer_ptr++ = buffer[i];
        }

        buffer_ptr -= (count - 1);

        return count;
    }
}