Projekte für das Labor des Moduls Rechnerorganisation der HTW-Berlin
im Studiengang Computer Engineering

Konrad Münch 2020

1. Labor:
Mit dem Tool Cube IDE der Firma STM32 arbeiten
insbesondere dem HAL.

- LED auf dem Board ansteuern und zum blinken bringen.

- Über den USART den String "Hello-World" ausgeben.

- Die LED-Leiste des HAT-Boards ansteuern und die acht LEDs 
  wie einen Scanner laufen lassen.

2. Labor

- Photowiderstand des HAT Boards nutzen und in Verbindung den Piezo-Buzzer
  ansteuern
 
- Spannung der Boardeingänge messen und über USART ausgeben

- Spannungsabfall über Photowiderstand feststellen

3. Labor 

In Assembler (ARM Assembler) das Labor 1 realisieren

- LED soll periodisch blinken

4. Labor

Mit Assembler und C eine Stopuhr realisieren welche direkt mittels Interrupts
Stop- und Startzeiten errechnet nach Betätigung eines Tasters auf dem HAT-Board.
Die gemessen Zeit soll über den USART in einem String versendet werden.
Genutzt wurden die Prozessorregister um die Berechnung zu realisieren. 
 
